\section{Background and State of the Art}
\label{sec:back}
{\color{black}Among the attack vectors based on collateral information leakage, timing attacks require special attention due to the high risk level they present on interconnected systems. An attacker with user permissions can perform a timing analysis on a specific task to extract secret information (e.g., cryptographic keys) remotely, without requiring physical access to the target device in comparison to other attacks. In this section, we first introduce the main characteristics of those timing SCAs in Section~\ref{sec:back_bernstein}, and then present the state of the art on protection methods in Section~\ref{sec:back_stateart}.


\subsection{Side-Channel Timing Attacks}
\label{sec:back_bernstein}
Time based SCAs leverage the dependence between (a) the operation of secret data, where the term `secret' refers to any un-encrypted data, key or information unknown by arbitrary users, with (b) the execution time, or operation latency, of the task using the secret data as an input.
Time dependence is a by-product of the operation from two sources; (i) in-processor or accelerator execution of the algorithm (e.g., cryptographic encryption) and (ii) memory access latency. Leakage from both sources can be mitigated by designing time-constant algorithms, basing the logic operations on constant latency instructions with no latency-variant branches, while constraining memory allocation within the same cache level so all data access have an identical time cost.
Full compliance with these statements limit the quality of the algorithm (e.g., encryption complexity), reduce the compatibility by targeting specific platform characteristics (e.g., cache capacity, instruction latency), and may hold back performance against using optimal operations (e.g., disable L1 cache). 
Moreover, some other SCAs would still be possible by, for instance, learning the cache sets or DRAM banks~\cite{DRAM1, DRAM2} accessed by the protected algorithm.
Thus, industry has opted for design policies and certification instead of a single air-tight solution, providing diversity in implementation with low risk of single point failure from a security standpoint.

Statistical analysis of the execution times has proven to be effective regardless of the leakage source.} As example, this study uses Bernstein's attack as a base, whose source code is publicly available~\cite{SCA_BERNSTEIN}.
Operations with secrets are identifiable, due to non-constant-time operations and/or non-constant data access latencies, through a classification and correlation of the average encryption timings from two data samples, with a known (attacker) and unknown (victim) cryptographic keys respectively. In particular, the timing measurements are categorized by the value of the plaintext bytes being encrypted in byte segments, which for AES-128 makes a total of 4096 individual metrics (256 possible values for each of the 16 plaintext bytes), taking advantage from the internal byte-wise operation, a common characteristic among cryptographic ciphers. 
{\color{black}This segmentation allows the reduction of the total number of encryptions, or \emph{sample size}, required by the attack to obtain a clear profile of the timings for each plaintext byte value. Furthermore, the samples obtained by measuring the duration of the cryptographic operation must be from a randomized input, referring to the plaintext in encryption or ciphertext in decryption, avoiding producing timing data with dependencies on the attack itself instead of the target operation.

\begin{figure}[t]
  \centering
  \includegraphics[width=1\columnwidth]{img/sca_cloud.png}
%  \vspace{-0.2cm}
  \caption{{\color{black}Average encryption timings, in clock cycles, with plaintext byte position 11 values listed by the x-axis. Data obtained from modified Bernstein's SCA with a sample size of $2^{27}$ encryptions for each known and unknown keys. Data values used by the SCA correlation are marked by arrows.}}
  \label{fig:sca_cloud}
  \vspace{0.1cm}
\end{figure}

As a practical example, Figure~\ref{fig:sca_cloud} shows the plaintext encryption timings of a SCA in an unprotected system. The SCA uses a sample size of $2^{27}$ encryptions for each key, for specifically the byte position 11. In detail, Bernstein's SCA correlation is a simple observation step, leaving aside standard error calculations, where the most distant timings from the average, around 732.25 clock cycles in the example, are taken as usable data. The idea is that these plaintext values, 10 and 11 for the unknown key and 254 and 255 for the known key marked in the figure, incur in an equivalent systematic latency overhead during the cryptographic execution. Hence, they can be correlated. Since the cipher base operation is the logic \emph{exclusive or} $\oplus$, the attacker is able of producing a list of candidates for the key to discover by operating with the same operation between the known key and the usable plaintext values.

The actual equation used is $K_{b}' \oplus P_{b}' = K_{b} \oplus P_{b}$, where $K_b$ is the key byte value, $P_{b}$ is a plaintext byte value, $b$ the byte position, and the $'$ apostrophe indicates to be from unknown key byte value or plaintext.
Note that the exclusive or neutral element in $\oplus$ is 0 ($x \oplus 0 = x$), therefore, using a zero for the known key simplifies the operation to $K_{b}'|_{K=zero} = P_{b}' \oplus P_{b}$. Hence, all experiments presented in this paper use a zero known key. Following the figure example, the candidates the SCA produces for the unknown key byte position 11 are found as $10 \oplus 254 = 244$, $10 \oplus 255 = 245$, $11 \oplus 254 = 245$ and $11 \oplus 255 = 244$, finding 7 out of 8 bits from the actual unknown key byte position 11, whose value is $244$. Note that each byte position has its own timing profile. Therefore, the same operations are made for each byte position.
%\begin{equation}
%  K_{b}' \oplus P_{b}' = K_{b} \oplus P_{b}
%  \label{math:xor_correlation}
%\end{equation}
%\begin{equation}
%  K_{b}'|_{K=nonce} = P_{b}' \oplus P_{b}
%  \label{math:xor_correlation_simple}
%\end{equation}

%after $2^{27}$ encryptions for both key samples, where a correlation can be made between the values 10 and 11 from the unknown key sample with bytes 254 and 255 from the known key sample. 
%{\color{red}(JA: a partir de aqui me cuesta seguir el parrafo. Ve paso a paso poniendo la operacion que se hace, lo que se obtiene y que significa)} The unknown key candidates can be produced from this correlation by operating with bit-wise exclusive or (XOR) each combination of highlighted values from both samples and the known key byte position 11, same as the samples. To facilitate the operation, nonce are used as known key due to being the neutral value in the XOR operation, resulting in 244 and 245, matching with the unknown key byte position 11 with value 244.

%Note that the timings presented in the figure have been centered over the global time mean for the byte {\color{red}(JA: que quiere decir centered aqui? Normalized? Si es asi, no se entiende que empiecen en cero en vez de en uno)}, or average encryption latency, since the attack specifically searches the values most distant from the global average to be used in the correlation.
SCA sample sizes of a low number of encryptions will provide a noisy cloud of timings, with high dispersion and difficult correlation. Thus, for the attack to succeed, it is crucial to work with large sample sizes, so the values with particular higher or lower average encryption times move far away from the average, reducing the number of dots to be correlated. Modifications made to Bernstein's attack for our study are listed in Section~\ref{sec:contr_sca}, while further practical details are explored in Section~\ref{sec:eval_cache}.


\subsection{State of the Art}
\label{sec:back_stateart}
Invulnerability against timing SCAs is challenging to achieve since several factors and components interact in non-obvious ways. Solutions to prevent timing SCAs can be categorized into two branches: (i) implementing time-constant cryptographic operations, and (ii) uncorrelating the access latency during operation.}

Time-constant operations executed by processors require compliance with design policies for cryptographic security, such as RISC-V ISA extensions~\cite{RISCV_AES_SPEC_SCALAR, RISCV_AES_SPEC_VECTOR}, which have already been applied in a practical implementation~\cite{RISCV_AES_IMPL}. Accelerator and discrete co-processor solutions, such as TPMs, are also included within this time-constant category, where the security and performance trade-off is apparent due to lacking full time-constant compliance for some products~\cite{TPM_SCA_FAIL}.
Software solutions also tend to focus on the operation latency, such as compiler optimizations~\cite{PROT_COMPILER} for avoiding branch prediction and instruction cache attacks that may present non-constant timings, and hence, a side-channel leak.
%These countermeasures focus on the logic operations to be time-constant, but do not ensure memory access to be completely free from timing pattern correlation, making them susceptible to the attack, thus, being the focal point of our study.
{\color{black}Even if the cipher algorithm was designed to comply with constant-time requirements, fitting within the lowest cache layer, an SCA could still occur by forcing specific cache evictions between the timed cryptographic operations, exposing secret data through cache misses, hence leaking side-channel information. This paper aims at counteracting SCA by focusing on the data access latency.}

Uncorrelation of the operating data and access latency, to avoid timing pattern identification of secrets, can be achieved by modifying the replacement policy of the data with custom cache implementations~\cite{PROT_CACHE, PROT_CACHE2}, forcing SCA data samples to diverge due to timing diversity, making them uncorrelated, hence, protecting secret data. 
{\color{black}In detail, the address allocation of the application are encoded with a randomized seed, using the resulting encoding for allocating the data in the cache sets. In order to make the protection effective, it is necessary to ensure the seed is randomized periodically, not to let the attacker learn. Such re-randomization needs to be performed by the Operating System (OS), by the user program or, periodically in an automated manner.
One of the key characteristics of these solutions is that the amount of timing data that can be used for correlation purposes by the attacker is proportional to the time a given seed is used for both keys without re-randomization. Therefore, an attack can only be successful if the time elapsed between seed updates is long enough to collect a sufficiently large sample to learn from.}

In our case, we investigate the effectiveness of a less intrusive hardware solution based on the integration and software programming of the SafeTI traffic injector for protecting against timing SCAs. While the SafeTI is not particularly suited for security purposes, it is a flexible and programmable component that could be leveraged for multiple functions, such as providing protection against multiple attacks, as well as for platform testing.


\section{Case Study Framework}
\label{sec:frame}

This section introduces the platform used as research vehicle for our work, as well as the SafeTI traffic injector used to counteract Bernstein's SCA.

\subsection{SELENE MPSoC Platform}
\label{sec:back_selene}
The SELENE platform considered in this work~\cite{SELENEplatform} has been released fully integrated and as an open-source platform usable on FPGA~\cite{SELENEgit}. It is based on Frontgrade Gaisler's technology including its 64-bit NOEL-V processor cores\cite{NOELV}, as well as the Advanced Microcontroller Bus Architecture (AMBA) Advanced Peripheral Bus (APB) and Advanced High-performance Bus (AHB) interconnects, and L1 and L2 caches, which are integrated from Gaisler's GRLIB IP~\cite{GRLIB}.

\begin{figure}[t]
  \centering
  \includegraphics[width=0.9\columnwidth]{img/platform.png}
%  \vspace{-0.2cm}
  \caption{SELENE platform schematic including SafeTI integration.}
  \label{fig:platform}
%  \vspace{-0.2cm}
\end{figure}

For this case study, we have integrated a SafeTI module targeting the shared L2 cache level in a SELENE instance with 4~NOEL-V cores (see  Figure~\ref{fig:platform}). The 4-way 32~B line 512~KBs L2 cache includes a pseudo-Least Recently Used (pLRU) replacement policy and keeps coherence within the core cluster. The L2 cache is also connected to the off-chip DRAM through an AMBA Advanced eXtensible Interface (AXI) and a memory controller. Each NOEL-V core integrates two individual L1 caches, for instruction (IL1) and data (DL1), of 4~ways and 16~KBs each, implementing a LRU replacement policy, {\color{black}and write-through policy with bus-snooping and an equal cache line size of 32~B to maintain coherence with the L2 cache.}
Both pLRU and LRU replacement policies offer vulnerabilities in front of SCAs due to their systematic eviction patterns, which attackers can leverage to alter the latency of data allocation during the cryptographic task.


\subsection{SafeTI Traffic Injector}
\label{sec:back_safeti}
The SafeTI is an open-source hardware component, created in our research group, devised as a flexible, portable and programmable traffic injector~\cite{SafeTIgit}, developed in VHDL. It is AMBA AHB compatible, and we foresee making it also AMBA AXI compatible in the near future.
The SafeTI is programmed through its integrated AMBA APB interface using 32-bit descriptors, which are stored within the internal descriptor buffer, made user-friendly through the public drivers along with the component designs.

Traffic injection is effectively limited by the throughput capable to be generated at the target interconnect. However, if such traffic is injected by software means through processor cores, it is further limited by the transaction size allowed by cores (either a double-word or a cache line) and controlled indirectly by inducing specific hardware behavior with a sequence of software operations.
Conversely, the SafeTI can inject precisely any traffic pattern, programmed as a set of descriptors, that the target interconnect accepts, including varying size data requests (e.g., from 1 byte up to 512 MBs), with/without burst mode, read/write, etc., and even introduce specific cycle-accurate delays between traffic injections. Hence, the SafeTI offers the flexibility and controllability needed for our work.

{\color{black}Compared with some previous solutions, SafeTI programmability permits tailoring it for different applications, compatible with virtually any cryptographic algorithm or vulnerable process to timing SCAs under certain conditions (e.g., shared L2 cache access). Integrability is supported by being a standalone module, making it suitable for other platforms as long as it is included with a compatible interface for targeting the desired interconnect. Further discussion is provided in Section~\ref{sec:disc}.}
