\section{Empirical Assessment}
\label{sec:eval}

\subsection{Evaluation Framework}
The experiments and data presented in this paper have been produced from software executions on a bare-metal synthesis of the MPSoC SELENE hardware platform on the Xilinx Virtex UltraScale+ VCU118 FPGA-based evaluation kit~\cite{FPGA}, operating at a frequency of 100 MHz. 
{\color{black}Software programs executed on the cores have been written in C and compiled with Frontgrade Gaisler AB’s NCC GCC Bare-metal toolchain version 1.0.4 on a Linux system with an O2 optimization level for a RISC-V target. Programs are loaded into memory using the FPGA debug software GRMON3~\cite{GRMON} with the main core set with a specific pointer during the platform booting, matching the program compilation pointer. All secondary cores are left disabled to provide a noise-free environment for the experiments.
The program software includes the modified Bernstein's timing SCA targeting AES-128 ECB cryptographic cipher. Specifically, the attack targets low-level encryption operations of randomized plaintext.

The evaluation presented in this paper is divided per each type of countermeasure tested as follows: 
\begin{itemize}
\item Disabling platform caches (Section~\ref{sec:eval_cache}).
\item Evicting \emph{Te} tables by segments in every time interval (Section~\ref{sec:eval_blocks}).
\item Evicting a single \emph{Te} table every time interval (Section~\ref{sec:eval_singletable}).
\item Evicting a combination of \emph{Te} tables every time interval or alternatively in every time interval (Section~\ref{sec:eval_multtables}).
\end{itemize}


\subsection{Cache Disabling as Countermeasure}
\label{sec:eval_cache}
Obtaining a base reference of the timing SCA effectiveness on this case study environment is imperative in order to provide a contrasted view of the protection achieved by the SafeTI in the following sections. Therefore, this section presents and reasons about the SCA capabilities in four different environments where DL1 and L2 caches can be enabled or disabled, namely, when both are enabled, when only DL1 is enabled, when only L2 is enabled, and when both are disabled.}
%at the context where the SELENE platform caches DL1 and L2 are enabled, disabled alternatively and both disabled, providing useful information of the attack.

\begin{table}[t]
  \centering
  \caption{Number of bits discovered by the SCA\\against alternative cache compositions}
  \begin{tabular}{|*{7}{c|}} \hline
  \multicolumn{2}{|c|}{Enabled caches} & \multicolumn{5}{c|}{SCA sample size (encryptions)} \\ \hline
  \hspace{.8mm}DL1\hspace{.8mm} & L2 & 2\textsuperscript{21} & 2\textsuperscript{22} & 2\textsuperscript{23} & 2\textsuperscript{24} & 2\textsuperscript{25} \\ \hline
  \checkmark    & \checkmark  &    2.9 &   14.3 &   33.3 &   70.6 &   72.2 \\ \hline
                & \checkmark  &    0.0 &    0.0 &    0.0 &    0.0 &    0.0 \\ \hline
  \checkmark    &             &    3.7 &   14.9 &   28.1 &   89.3 &   92.0 \\ \hline
                &             &   41.8 &   64.2 &   72.6 &   80.0 &   80.0 \\ \hline
  \end{tabular}
  \label{table:sca_cache}
\end{table}

%{\color{blue}A brute force attack on the 128-bit key would require exploring up to $2^{128}$ key combinations to discover the key. For simplicity, we refer to this case as a case where the attacker has to discover 128 bits. As the SCA becomes increasingly effective, some byte values for part of the key are discarded, and fewer combinations need to be tested. We express the total number of remaining key combinations to explore with the equivalent number of undiscovered bits to ease reading. For instance, given a key of 16 bits, hence consisting of 2 bytes, if the attack narrows down the value of the key to 37 out of 256 values for one byte and 59 for the other, the subsequent brute force attack would require exploring $37 x 59 = 2,183$ key combinations (instead of $2^{16}$), which we express as having to find $11.1$ bits (i.e. $log_2(2183)$), or equivalently, as having found $4.9$ bits.}

Table~\ref{table:sca_cache} shows the equivalent number of bits found by the attacker (out of the total 128 bits of the key) for several sample sizes of the attack in our evaluation platform. These values must be read as follows: if the attacker discovers $X$ bits of the key, it would need a brute force attack exploring $2^{128-X}$ key values. Note that, in practice, the attacker does not discover specific key bits but discards byte values for different parts of the key. However, we represent results as the number of key bits that would need to be discovered to match the same cost of a brute force attack to facilitate understanding.
{\color{black}For instance, given a key of 16 bits, hence consisting of 2 bytes, if the attack narrows down the value of the key to 37 out of 256 values for one byte and 59 for the other, the subsequent brute force attack would require exploring $37 x 59 = 2,183$ key combinations (instead of $2^{16}$), which we express as having to find $11.1$ bits (i.e. $log_2(2183)$), or equivalently, as having found $4.9$ bits.

Starting from the base cache composition of DL1 and L2 caches enabled, the SCA results correspond with the information presented in previous sections. The attacker is capable of discovering more bits of the unknown key by increasing the sample size of the attack, since that way, it is capable of reducing the number of plaintext values highlighted by a higher or lower average encryption times compared to other plaintext value timing averages.}

Disabling the DL1 cache but maintaining the L2 cache enabled shows that no information has been discovered by the timing SCA, therefore, we learn 2 things. First, all memory accesses from the AES-128 CBS algorithm have an equal operation latency in this setup. This evidence proves data access latency is the exclusive leakage source from the cryptographic operations being timed, at least on the SELENE platform. Second, not finding timing differences between plaintext values, even when increasing the SCA sample size, indicates all data accessed by the SCA fits within the 512~KBs of the L2 cache.

{\color{black}The complementary case where the DL1 cache is enabled but the L2 cache is disabled denotes slightly more susceptibility to the SCA than the base cache configuration. 
In this case, DL1 cache misses, instead of hitting in L2 cache, need to access main memory, whose latency is higher than that of L2 cache hits. Hence, those timings that were discrepant in the setup with both caches enabled become even more discrepant when the L2 cache is disabled. 
%A possible reasoning is that, due to memory access has greater latency than a hit on L2 cache as in the base case, the timing for operations whose perform more misses on DL1 due to specific operands incur on timings more discrepant than with others plaintext values. 
Therefore, larger differences provide easier correlation, which in turn provides a smaller list of unknown key candidates, or what is equivalent to, a higher discovery rate.

Last but not least, the disabled DL1 and L2 caches configuration shows to be the most susceptible to the attack from all cache combinations with small samples. Initially, one could expect this case to behave similarly to the prior case with disabled DL1 and enabled L2 caches, given that all data accessed by the timed operations fits within the external DRAM. However, these off-chip components introduce data and access latency dependencies~\cite{DRAM1, DRAM2}, leaking side-channel information whereas the previous case where DL1 is disabled and L2 enabled provides homogeneous latency for all L2 cache hits. In detail, there is an access latency difference when accessing depending on the data accessed due to bank and rank access patterns, generating a data-dependent timing profile due to specific plaintext encryption values requiring extra latency for their accesses than others.}

From a countermeasure perspective, the disabled DL1 and enabled L2 cache composition is a strong contender as a protection solution against Bernstein's timing attack. However, reducing cache levels results in higher data access latencies, increasing the average encryption time, from the 730 clock cycles of the baseline case (both caches enabled) to 4,763 clock cycles, a considerable 552\% overhead.
Moreover, it could be argued that a smart attacker may be able to re-enable the attack by evicting parts of the data used by the cryptographic operation to highlight the use of specific data, hence enable correlations in the timing behavior of the cipher algorithm since the accesses latency would depend on the plaintext.


\subsection{Partial Table Eviction with SafeTI}
\label{sec:eval_blocks}
{\color{black}The first countermeasure method using SafeTI for evicting data from the L2 cache consists of evicting AES Te tables, block by block, but only evicting one segment in every time period. For instance, if a table occupies 1~KB of cache space, and it is divided into 4 blocks of 256~B each, SafeTI evicts bytes 0-255 in period $P$, 256-511 in period $P+1$, 512-767 in $P+2$, 768-1023 in $P+3$, 0-255 again in $P+4$, and so on and so forth.
% of the AES Te tables {\color{blue}sequentially} at a constant time period, {\color{blue}evicting complete tables block by block asynchronously}. 

Due to SafeTI's limited descriptor buffer, the injection patterns tested have been constrained to the following 4 different cases: Te0 eviction by 64~B blocks; Te0, Te1 and Te2 eviction by 128~B blocks; all Te tables eviction by 256~B blocks; and all Te tables eviction by 512~B blocks. Between each evicted block, the injection pattern includes a stand-by time in clock cycles, which we refer as \emph{Delay}, that is constant for each experiment so that evictions are homogeneously distributed over time.

Regardless of the Delay or table/s eviction granularity, experimental results show that this protection method is ineffective at counteracting the SCA. The Delay values tested are in the range between $10^3$ and $10^5$ clock cycles.
These values allow full tables to be evicted at the same frequency as the best cases for subsequent experiments where tables are evicted at once instead of block by block.
Results show a similar discovery rate of the key by the attack among all 4 protection cases.

Furthermore, the SCA is slightly more successful with this approach than for the base SCA without protection. This negative effect (i.e., the protection helps the attack rather than counteracting it) relates to the fact that the data accessed from the cryptographic operation depends on the plaintext. Hence, evicting single blocks of the Te tables only highlights such plaintext values that access the recently evicted cache lines. Therefore, the attacker learns faster and injection patterns evicting full tables at once are expected to cure this anomaly as analyzed next.}


\begin{figure}[t]
  \centering
  \includegraphics[width=1\columnwidth]{img/safeti_delay.png}
%  \vspace{-0.2cm}
  \caption{Remaining key combinations from several SCAs with single table eviction protection, including different Delay values, from $2^{23}$ to $2^{26}$ encryption sample sizes.}
  \label{fig:safeti_delay}
%  \vspace{-0.2cm}
\end{figure}

\subsection{Table Eviction with SafeTI}
\label{sec:eval_singletable}
The injection pattern for the experiment in this section is analogous to that of the previous section, but with the evicted block matching the table size {\color{black} of 1~KB}. Hence, in every period the target Te table is evicted. Then, SafeTI stands by for Delay clock cycles before looping again.

Figure~\ref{fig:safeti_delay} shows the result of the SCA while a full Te table eviction protection is in place (for Te0 in particular), for different sample sizes for the SCA (between $2^{23}$ and $2^{26}$ sample sizes), varying the Delay between full-table evictions.
We reach the following observations:
\begin{itemize}
%\item Results are noisy due to the statistical nature of the learning process of the SCA, and the intrinsic variability in the execution time measurements of the samples.
\item Results are noisy {\color{black}due to minor modifications in the source code, presenting an intrinsic variability in the execution time measurements and SCA results (shown in Figure~\ref{fig:sca_filler}) as we discuss next.}
\item There is a (central) range of Delay values for which the SCA is unable to learn anything about the key so that the number of potential key combinations to explore by brute force remains at $2^{128}$. However, as the sample size increases, such Delay range narrows down. If the sample is large enough, as we show in later experiments, the range becomes null and the SCA starts learning about the secret key regardless of the Delay value. Still, there is always a particular delay minimizing the amount of information learnt by the SCA.
%presenting the 2 following subjects: the figure's data is noisy, like tracing multiple curves at the same time, specially whenever full protection ($2^{128}$ key combinations) is not achieved; and protection effectiveness depends on the Delay value for both on information leak and sample size of the attack, indicating there is an optimal Delay value for maximizing the SCA sample size range where the protection resists leaking any data.
\end{itemize}

{\color{black}During our experiments, we noticed that small variations in the code created significant variations in the results for a given Delay and sample size, and concluded that the particular cache alignment of the data has an impact on the results in absolute terms, yet trends hold. 
This is illustrated in Figure~\ref{fig:DL1sets}, which represents two sets from a 4-way cache, where there is data in static addresses allocated during compile time, and data in dynamic addresses allocated during runtime (e.g., in the stack frame), both marked with \emph{s} and \emph{d} suffixes respectively. The data is ordered from most recently used \emph{A} to least recently used \emph{E}. Focusing on the first case without a filler size (i), the set 0 caches sA (spanning across two cache lines), dC, dD and dE data lines, but once a filler size is applied to displace dynamic data by one set at (ii), dE is no longer able to fit within the cache, illustrating why some pointer displacements are able to leak more information than others.

%Starting with the noisy data, after several tests it was confirmed SCA effectiveness has a diversity range depending on address allocation set at compile time, thus, influencing cases where the protection, if any, is defeated. 
Figure~\ref{fig:sca_filler} shows both the protected (straight lines) and unprotected attacks (dashed lines) for several compilations of the same program (cipher and attack) but with different \emph{filler sizes} (between 512~B and 8~KB), which is an unrelated data array used to shift the cryptographic operation pointers for each experiment. The figure shows the diverse results in the unprotected case, ranging between $2^{25}$ and $2^{57}$ unknown key candidates, with a sample size of $2^{27}$ encryptions. In the protected case, variability is drastically decreased, partly because few key combinations are filtered out by the attack.  
%ranging between $2^{25}$ and $2^{26}$ encryptions, for pointer displacement of 512~B, 1, 2, 4~KBs. 
Note that, whenever the filler size is a multiple of the DL1 way size (4~KBs), such as 4 and 8~KBs, results remain the same, confirming the DL1 set-mapping influence over the SCA.
%This effect can be better understood if the attack is recognized as an study of the 16~KBs (DL1 total cache capacity) most used by the cryptographic operation, while taking into account the DL1 set-associative mapping. 
In any case, no array for shifting pointers has been used for the remaining experiments in this paper.}

\begin{figure}[t]
  \centering
  \includegraphics[width=1\columnwidth]{img/sca_filler.png}
  \caption{{\color{black}Remaining key combinations from unprotected and protected SCAs with Te3 table eviction and optimal Delay of $2x10^5$ clock cycles for different sample sizes, and varying filler sizes shifting the compile address alignment of useful data.}}
  \label{fig:sca_filler}
\end{figure}

\begin{figure}[t]
  \centering
  \includegraphics[width=0.9\columnwidth]{img/DL1sets.png}
  \caption{{\color{black}Associative-set mapping diagram from a 4-way cache in two instances of (i) no filler size and (ii) filler size with one set of displacement. Data is named after being statically \emph{s} or dynamically \emph{d} allocated, from most used \emph{A} to least \emph{E}.}}
  \label{fig:DL1sets}
  \vspace{-0.2cm}
\end{figure}

%{\color{blue}(FF: La siguiente explicación es larga, a lo mejor es necesario resumirla. Quizás mover el concepto de la lista de los 16KBs más usados a donde se explica el SCA en background?)}
%To understand this effect, it helps to view the SCA as an study, instead of the timings, of the 16~KBs (DL1 total cache capacity) of data used the most by the cryptographic operation targeted by the attack. Since the platform is vulnerable due to the DL1 cache existence between the core and the L2 cache, where the data is completely cached as proven in Section~\ref{sec:eval_cache}), it can be assumed the set-associative mapping of the DL1 cache may affect the SCA timings due to altering the DL1 hits or misses.
%For instance, at a low SCA sample size of $2^{21}$ encryptions, the attacker is able to produce a noisy timing sample, which could be translated to a rough estimation of the most used data that does not provide high correlations yet. Increasing the sample size to $2^{27}$, highlighted timings appear thanks to producing a better estimation of the data most used, which would be of lower times from higher hit rate on DL1. If the accessed data is displaced by, an array size modification (filler size array) for example, the profile of most used data losses validity since the data may no longer be allocated within the same sets as previously. Therefore, the SCA has a slight dependency on the pointer position of the data accessed by the cryptographic operations. However, this fact does not change the tendency where the attacker can still increment the unknown key bits discovered by increasing the sample size. This statement can be further proved by testing with filler sizes with offset by multiples of 4~KB, the DL1 cache way size, such as 4 and 8~KBs which land with the exact same results due to repeating the DL1 set allocation between experiments.

%Unless a filler size is mentioned, 
%In any case, no array for shifting pointers has been used for the remaining experiments in this paper.
%, though it should be considered that any SCA may include some slack in the unknown key discover rate as result of this effect.

As shown before in Figure~\ref{fig:safeti_delay}, the degree of protection achieved depends on the Delay value, or eviction period. Such evictions aim at generating arbitrary noise able to remove any correlation that could be used by an attacker. 
If performed with the right periodicity, evicting a Te table from the L2 cache causes L2 misses, and hence, access latency increases (and so execution time increases) arbitrarily and with enough magnitude to surpass the execution time variability caused by the underlying access patterns that the attacker is trying to learn.
%In the optimal conditions, evicting a Te table from the L2 cache introduces random noise in both the timings and its equivalent of most used data estimation. The noise is randomized due to several random factors, such as randomized data access, derived from the plaintext randomization, and the DL1 data caching, which depends on previous operations due to the pLRU replacement policy and limited capacity.
For instance, a DL1 cache set may contain a Te table line or not depending if it has been recently used. Given that the plaintext encrypted in the recent past has determined, along with the pLRU replacement policy of the DL1 cache, what lines of the Te tables are stored in DL1, Te lines retrieved from L2 are, to some extent, arbitrary. Hence, when those accesses experience higher latencies due to L2 misses caused by SafeTI evictions is, therefore, highly arbitrary. This makes execution times be apparently random because the level of noise introduced is high enough and, apparently, uncorrelated with the key.
However, if the eviction period is too small, DL1 misses also miss in L2 highly systematically, which makes overall execution time increase, but noise be low. Similarly, if the eviction period is too high, meaning that evictions only occur seldom, the protection effect SafeTI has on the SCA is very limited.

%this in effect is random due to the randomized input, the latency increase due to L2 miss product of L2 cache eviction forced by SafeTI occurs at a random time, only dependent on the previous and the operation that accesses such line of data.
%the SCA learns from usually missing on L2 and hitting on DL1 if the access is repeated in the same line during a specific plaintext encryption. Otherwise, if the eviction period is too high, 
%, being defeated by increasing the attack sample size.

\begin{figure}[t]
  \centering
  \includegraphics[width=1\columnwidth]{img/safeti_singletables.png}
  \caption{{\color{black}Remaining key combinations from protected SCAs with Te0, Te1, Te2 or Te3 table eviction and optimal Delay, in order to maximize protection, for each table at different sample sizes.}}
  \label{fig:safeti_singletables}
  \vspace{-0.35cm}
\end{figure}

Finding the optimal Delay for the protection is challenging, due to a dependence with collateral data being evicted from the same sets where the target Te is cached. This makes, in fact, that the optimal Delay varies across Te tables, as shown in Figure~\ref{fig:safeti_singletables}. 
%shows for each Te table with its optimal Delay value each, 
Therefore, the only method available to optimize the Delay and choose the value that maximizes the sample size needed by the attacker is through empirical testing. As shown in the figure, the degree of protection achieved across the different tables, even for near-optimal Delay periods, may also vary. For instance, Te1 periodic eviction provides slightly higher protection than that achieved by evicting other tables due to the interactions with other data of the cipher program. Yet, these results also depend on the program pointer shift as shown before in Figure~\ref{fig:sca_filler}.

Overall, the single table eviction protection, once adjusted with the optimal delay, maintains zero side-data leak up to an attack sample size no lower than $2^{25}$ with an average encryption time of 741 clock cycles, 
x32 times the attack sample size at the cost of 1\% increase in average encryption latency when compared with the unprotected SCA.
%Compared with the unprotected SCA, it achieves full obfuscation for x32 times the attack sample size at the cost of a negligible 1\% increase in average encryption latency.


\subsection{Multiple Table Eviction with SafeTI}
\label{sec:eval_multtables}
{\color{black}This section extends the injection patterns, considering cases where all tables are evicted rather than focusing on one of them. The goal of evicting all tables rather than the very same one systematically is introducing higher entropy, and hence, further challenging the attack.
%deriving in full Te0 to Te3 tables eviction, intending an entropy increase in the timing data produced by the SCA.

%The eviction of a single table is still vulnerable due to a simple reason, the SCA is learning what is the most used data by every cryptographic operation. For example, if a specific plaintext encryption accesses two times to the same Te cache line, the protection is unable of interfering on the second access, thus, the attack is able to produce a correlation from a ensured L1D hit with a high sample size.
%However, if the eviction is expanded to all Te tables, the protection may still not be able to avoid this type of correlation, but introduces more cases where data may or may not be cached at any level, forcing a higher dispersion of the timing data produced by the SCA, therefore, requiring a sample size increase.
%Furthermore, if each table eviction is spliced, interspersed by a stand-by Delay time in between each, the SCA correlation difficulty increases substantially due to occurring at different instances following a similar reasoning.

\begin{figure}[t]
  \centering
  \includegraphics[width=1\columnwidth]{img/protected_comparison.png}
  \caption{{\color{black}Remaining key combinations from unprotected and protected SCAs using all 4 methods presented in this paper at different sample sizes.}}
  \label{fig:safeti_tables}
\end{figure}

Figure~\ref{fig:safeti_tables} shows a summary of all SCA cases presented in this paper, focusing on the most favorable setups identified in each case, including scenarios where all tables are evicted. In particular, the configurations evaluated are as follows:
\begin{itemize}
\item Unprotected SCA (dashed purple line).
\item Te3 eviction by 64~B blocks every 400 clock cycles (red line).
\item Full Te3 table eviction every $2x10^5$ clock cycles (green line).
\item All Te tables evicted simultaneously every $2x10^5$ clock cycles (orange line).
\item Same as previous one, but instead of evicting all Te tables at once, we evict them in an interleaved manner so that one Te table is evicted every $5x10^4$ clock cycles (blue line), which matches the eviction frequency of the previous case where all tables are evicted every $2x10^5$ clock cycles.
\end{itemize}

Focusing on the patterns where we evict all tables, the orange line withstands full protection up to an attack sample size of $2^{27}$, with a 4\% average latency overhead, whereas the blue line keeps full protection up to an attack of $2^{30}$ encryptions, with a 12\% average latency overhead.
%{\color{red}(JA: solo para confirmar, las lineas naranja y azul echan todas las tablas a la misma frecuencia globalmente, pero su impacto en el tiempo de ejecucion varia del 4\% al 12\%, correcto? Sabemos por que?)}
% (FF: Naranja es 741, azul 762, una diferencia de 21 ciclos en promedio. Si el AHB bus es de 128 bits y la tabla es de 1 KB, se requieren 8 ciclos para acceder al mismo tamaño, y 32 para poderlo sacar de la L2 (no seguidos, se hacen de 8 en 8). En el caso de que CPU esté haciendo L2 misses de forma frecuente, que es lo que ocurre recien se hace evict de cualquier tabla, es probable que se encuentre el SafeTI accendiendo a la L2. Si el periodo es muy bajo, como es el caso del azul, este efecto se intensifica.)
Although one could expect the encryption latency overhead to be proportional to the number of L2 misses induced, and thus to the L2 cache evictions performed by the traffic injector, this is not the case between orange and blue lines, which correspond to an equivalent eviction rate, and thus, should cause a similar performance overhead. 
However, the case of individual table evictions at a higher frequency (blue line) turns out to perform the evictions clashing with the execution of the encryption function more often, and hence causing higher L2 cache access interference, and increasing encryption latency.
%The source of the discrepancy is the reduced eviction period of the interleaved eviction case (orange line), since synchronized collisions between the encryption process and SafeTI petitions are more frequent in lower Delay values, specially for eviction sizes that take several clock cycles.
}

All Delay values present in the figure have been empirically optimized, with the exception of the last case where we evict all tables in an interleaved manner, where the $2^{31}$ encryptions experiment duration has been 29~hours in our FPGA, making it unreasonable any further increase in the sample size. 
%The increase in latency is reasonable due to the higher quantity of evicted data at a lower rate.
