\section{Evaluation Platform}
\label{sec:platform}
To fully validate SafeTI features and have a proof of concept, we have integrated SafeTI on SELENE \cite{SELENEproject}, an MPSoC safety-critical platform based on Frontgrade Gaisler AB RISC-V NOEL-V cores \cite{NOELV}. 
This instance implements two levels of 4-way set associative caches and multiple networks following the standard AMBA 2.0 using several reusable VHDL IP cores by Frontgrade Gaisler AB. 
Figure~\ref{fig:platform} block diagram presents the relevant connectivity of the different hardware components in the platform for this research. 

\begin{figure}[t!]
  \centering
  \includegraphics[width=0.40\textwidth]{img/selene.png}
  \caption{SELENE MPSoC platform architecture with SafeTI integration at the AHB interconnect level.}
  \Description{SafeTI targets the L2 cache level of the platform, through the AHB interconnect, while being programmed through the APB interface, that at the SELENE platform, it is bridged from the same AHB interconnect. The L2 cache has access to memory through an AXI NoC, where accelerators might also be instantiated.}
  \label{fig:platform}
\end{figure}


\subsection{MultiProcessor System on Chip}
SafeTI is integrated and evaluated in an MPSoC instance implementing 6 Gaisler's NOEL-V 32-bit cores. 
Those cores are dual-issue, implement the RISC-V ISA, and each one includes private L1 data and instruction caches.
The L1 caches have access to an L2 cache and AHB subordinates, such as the APB arbiter through the AHB network. All AHB managers are multiplexed on the AHB network following a Round-Robin scheduling algorithm by an AHB arbiter module. 
In addition, an AXI4 network connects the L2 cache as a manager with the system memory of a total 2~GB capacity as a subordinate. Parallel to the AXI4 network, an APB network is used to program and communicate with different devices, including SafeTI.

Both AHB and AXI4 data bus widths implemented are 128-bits wide.
This topology has the advantage of a more straightforward structure while facilitating cache coherency between the two cache levels (e.g., cache lines can be transferred atomically). By contrast, the implemented APB network data bus is 32-bits wide, hence only able to transfer individual 32-bit words. 
Both interfaces, namely AHB and AXI4, alternate service between actors requesting data transfers, implementing a round-robin scheduling system, multiplexing data access fairly between CPUs and SafeTI.

Each core integrates a write buffer that decouples core instruction execution from delays experienced by write operations when attempting to access the network. Hence, delays caused by other managers when accessing a shared resource, also called contention, experienced by write operations only stall the core pipeline if the buffer is full and the core attempts to insert a new write request.
Hence, stalls occur only upon several consecutive burst operations that fill in the write buffer faster than it can be emptied due to other actors holding the network busy.
In the MPSoC used in this work, therefore, write requests caused by stores are non-blocking (i.e., the core pipeline is not stalled and can keep processing further instructions upon a store instruction execution), whereas read requests caused by load instructions are blocking (i.e., the next instruction is not executed until the requested data is not fetched).

Even though SafeTI's injection interface would also allow to mimic accelerator's traffic by targeting the AXI4 interconnect, we decided to focus first on the AHB network (and hence develop its AHB interface) due to two reasons; (i) the AHB interface allows SafeTI act as a manager for the L2 cache component, as any other CPU core, hence offering capabilities to send controlled traffic to the L2 cache for performance assessment or even validation purposes~\cite{SafeTIconf_DFT}; and (ii) since the SELENE platform does not guarantee data coherence between multiple devices connected to the AXI4 interface sitting between the L2 cache and main memory, at least in its GPL version, connecting SafeTI to the AHB interface offers a richer set of possibilities, including the generation of coherence-related activities. For completeness, later in the paper, we also consider the case where a SafeTI module is added to the AXI4 network and assess traffic injection in both networks independently as well as simultaneously. 

\begin{table}[t!]
  \centering
  \caption{Caches L1 and L2 characteristic features}
  \begin{tabular}{r|cc} \toprule
  Characteristic     & L1 cache                    & L2 cache        \\ \midrule
  Total capacity     & 16~KB x2\textsuperscript{1} & 512~KB          \\ 
  Associative level  & 4-way set                   & 4-way set       \\ 
  Way capacity       & 4~KB                        & 128~KB          \\ 
  Line capacity      & 32~B                        & 32~B            \\ 
  Replacement policy & LRU                         & pRandom or pLRU \\ 
  Write policy       & Write-through               & Write-back      \\ \bottomrule
  \end{tabular}
  
  \textsuperscript{1}The L1 cache integrates 16~KB read-only for instructions and 16~KB for data.
  \label{table:cache_features}
  \\
\end{table}

\subsection{Memory Hierarchy}
The platform incorporates two cache levels, L1 and L2 caches, and the main memory.
Each processor core has a dedicated L1 cache for data and instructions, while all cores share the L2 cache.
Table~\ref{table:cache_features} lists the featuring characteristics of both L1 and L2 caches for reference.

The main characteristics of the memory hierarchy implementation for our evaluation are; (1) the L1 data cache is write-through, meaning all write operations are forwarded to the L2 cache regardless of whether they hit in L1; and (2) the L2 cache has a write-back policy, where every L2 cache miss forces a main memory read, and, upon the replacement of a dirty line, a memory write in addition to the read to send the dirty line to main memory.

Regarding replacement policies, a characteristic that decides on the eviction of cached lines, the L1 cache has a fixed Least Recently Used (LRU) implemented. Meanwhile, the L2 cache can be implemented with pseudo-LRU (pLRU) or pseudo-random (pRandom) replacement policies, explored in the following evaluation.
The pRandom replacement policy uses an internal counter in the L2 cache that decides what line to evict during an L2 cache miss (L2MISS) access, providing the pseudo-random factor. The pLRU replacement policy is based on a bit tree.
Further information about the L2 cache can be found in~\cite{L2cache}.


\subsection{SafeTI Integration Footprint}
Hardware integration on an existing platform increases the implementation footprint, be it silicon area on ASICs or configurable logic blocks (CLB) on FPGA-based platforms. SafeTI integration on the SELENE platform in the FPGA-based platform used for our evaluation employs a total of 3810 configurable logic block look-up tables (CLB LUTs) and 9137 CLB registers. 
For perspective, the traffic injector represents 0.98\% CLB LUT and 4.93\% CLB registers of the total SELENE platform, or 0.32\% CLB LUT and 0.39\% CLB registers of to those available in the evaluated FPGA platform.
In terms of power consumption, a SafeTI module is estimated to consume 31~mW out of 3.4~W, below 1\% of the complete platform usage, by Vivado Design Suite v2020.2~\cite{vivado} under default operation conditions. However, the consumption is transient, depending on the traffic pattern programmed on the injector and the contention suffered during traffic generation due to other actors.

The relatively high usage of CLB registers may be adjusted by modifying the \emph{Descriptor buffer} used to store SafeTI descriptors. In particular, 90\% of the CLB registers used by SafeTI are devoted to implementing a buffer with 256 32-bit words of capacity, i.e., 128 read/write traffic descriptors. Such overhead is malleable by varying the size of the descriptor buffer and could be easily decreased if wanted.
In addition, the CLB registers usage could be diminished further by tailoring the \emph{Descriptor buffer} implementation for using specific FPGA resource \emph{block RAM tile}. Using NOEL-V total 32~KB of L1~cache, implemented by 22 block RAM tiles, as a reference, it can be deduced that allocating a single block RAM tile out of 2160 available in the FPGA may implement a \emph{Descriptor buffer} with a capacity of 372 32-bit words, or 186 read/write traffic descriptors. This would represent less than 0.05\% of the block RAM in the FPGA to implement an even larger buffer.
However, we decided to maintain the SafeTI implementation as generic as possible using CLB registers instead for increased compatibility and portability, avoiding possible integration limitations on alternative hardware platforms.
