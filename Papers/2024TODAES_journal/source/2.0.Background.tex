\section{Background \& Related Work}
\label{sec:back}
Simultaneous access to the interconnect (and the components reachable through such interconnect) in MPSoCs may lead to functional and timing interference. Therefore, V\&V of the impact of that interference becomes mandatory in the context of safety-relevant systems. 

Traffic injection, a usual approach to perform such V\&V activities, has tackled the challenge from different angles, including (i) automated test equipment, (ii) performance simulators, (iii) circuit simulators, (iv) hardware emulated injectors and (v) software stress tests.
This section reviews the state of practice for those alternative approaches.


\subsection{Traffic Injection with Automated Test Equipment (ATE)}
ATE is often used for post-silicon validation activities~\cite{ATE1,ATE2} injecting specific signals in the pins of the chip. However, since ATE needs to operate, at most, at the speed allowed by the pins of the chip, and those inputs need to be propagated down to the corresponding locations stimulated, input switching may easily occur at a lower frequency than the operating frequency of the device under test (i.e., the processor). Moreover, ATE is used mostly by chip manufacturers for chip validation due to its cost and infrastructure needed, not by chip users to analyze the performance of their applications. Hence, while ATE could be used for traffic injection for performance analysis, it is not a viable option in practice.


\subsection{Traffic Injection with Performance Simulators and Emulators}
Gem5~\cite{gem5} is a popular performance simulator for a wide variety of modern microarchitectures. Gem5 is compatible with multiple Instruction Set Architectures (ISAs). As part of Gem5, we can find Garnet~\cite{garnet}, an interconnect model, which comes along with a traffic injector capable of producing synthetic traffic for NoCs. Garnet, due to its software nature, is highly flexible and allows configuring a number of features such as the duration of the simulation, the traffic injection rate, source and destination for the traffic injected, etc. Some NoC-related works, such as SMART~\cite{smart_1} and OpenSMART~\cite{smart_2}, have built upon Gem5 and Garnet for their evaluation.

Analogously to Garnet, GMemNoCsim~\cite{gmemnocsim}, a powerful NoC simulator, also allows injecting synthetic traffic in the simulated NoC. GMemNoCsim also allows recording traffic patterns and using them for injection later on.
Synfull \cite{synfull} is a traffic injector for NoC simulators. In particular, Synfull targets cache coherence traffic. It builds upon traffic traces recorded previously with a performance simulator, which are compressed for efficiency reasons and injected later on.

Hardware emulators, such as, for instance, QEMU~\cite{QEMU}, allow modeling the whole system and provide relatively high emulation speed compared to performance simulators. However, hardware emulators generally aim at modeling the functional behavior rather than the timing behavior. Hence, they are not the type of platform where to model the effects of traffic injection.

Those tools offer some features compared to the ones of SafeTI, but they are software models, hence targeting traffic injection at the simulation level, either in limited-accuracy models (e.g., performance simulators), diverging from real SoC implementation constraints (e.g., memory access latency), or in precise models but with overwhelming simulation costs precluding the simulation of real use cases or applications. SafeTI, instead, is a hardware component synthesized along with the rest of the design so that it can be used for traffic injection in FPGAs or Application-Specific Integrated Circuits (ASICs) for commercial designs running real use cases or applications since SafeTI operates at speed.
For instance, in the scope of our evaluation, SafeTI injects traffic at 100MHz (the operation speed of the FPGA used). Instead, simulators would need to drastically decrease accuracy to inject traffic at a comparable speed (still much lower for any meaningful degree of accuracy of the system modeled) or inject it at a rate several orders of magnitude slower than in the case of SafeTI to use comparably accurate models.


\subsection{Traffic Injection with Circuit Simulators}
Circuit simulators also allow injecting traffic. For instance, Questasim~\cite{questasim} and Verilator~\cite{verilator} allow injecting test vectors flexibly for virtually any signal. However, this type of tool operates at very detailed abstraction levels such as VHDL, Verilog, SystemVerilog, or Register-Transfer Level (RTL), hence with simulation speeds even slower than those of highly-accurate performance simulators. Such simulation speed poses severe constraints on the tests that can be performed, particularly when large parts of the MPSoC should be simulated, too, for instance, to analyze the behavior of a multicore with shared caches and main memory.
In our case, SafeTI can be synthesized along with the MPSoC, enabling traffic injection to occur at speed, either in the FPGA or the ASIC.


\subsection{Traffic Injection with Hardware Emulated Injectors}
Xilinx has developed its AXI Traffic Generator (ATG)~\cite{ATG}. Such IP is a hardware module enabling the injection of traffic in AMBA AXI4 interfaces in FPGA and ASIC designs, hence at the same abstraction level as SafeTI. Both ATG and SafeTI have some similarities related to their flexibility and programmability to inject traffic. However, there are some differences in terms of their capabilities and usability.

For instance, in terms of usability, Xilinx's ATG licensing model is highly restrictive, and one of its constraints relates to the fact that the ATG is only allowed to be used on Xilinx devices. SafeTI, instead, comes along with an open-source permissive license (MIT), and requests for other types of license are intended to be accepted if eventually needed. 

Also, in terms of usability, ATG targets AXI4 only, hence limiting its use. Instead, SafeTI is not particularly constrained to any specific protocol. Our first incarnation has been tailored to work for the AMBA AHB protocol, and an AXI4 interface is expected to be released soon. Yet, other protocols (e.g., Tilelink) are not expected to pose any meaningful portability challenge.

In terms of capabilities, ATG aims to generate high-frequency traffic. SafeTI, instead, is not limited to such type of traffic and allows tailoring the speed at which traffic is injected by enabling the use of programmable stall periods across injected requests.

As part of the H2020 Mont-Blanc 2020 project~\cite{mb2020}, a trace injector for RTL prototypes has been developed. It allows injecting pre-recorded traces. To our knowledge, such IP is not public and lacks the flexibility and programmability offered by ATG and SafeTI to inject traffic patterns exercising tight control on the injection due to its trace-based nature that, ultimately, creates a dependence on the ability to fetch traces to later inject them, with potentially high latencies to fetch them is arbitrarily large.

\begin{figure}[t!]
  \centering
  \includegraphics[width=0.55\textwidth]{img/demo_waves.png}
  \caption{Histogram for synchronous (CPU) and asynchronous (DMA and ETH) traffic examples, including access size in bytes. Synchronous sources follow a pattern with strict characteristics, CPU accesses are limited to single cache lines in this case, while asynchronous include access size diversity.}
  \Description{CPU executes exclusively 32 bit accesses, leaving constant slots due to fixed timing specific to the core and latency due to lower cache levels. DMA can diversify the size of the access, but it takes the same pattern of read and write the same size due to being designed for that specific task. ETH can diversify both access size and pattern, but it lacks full control due to the interface and protocol specifications.}
  \label{fig:demo_waves}
\end{figure}


\subsection{Traffic Injection with Software Stress Tests}
Software programs, often referred to as stressing benchmarks, have also been used to generate on-chip traffic in COTS devices. 
Typically, those benchmarks produce specific patterns (e.g., accessing specific cache or memory locations) to induce traffic indirectly in on-chip interconnects~\cite{metrics,MikelEMSOFT,GabrielSBACPAD}. 
Those benchmarks bring two important advantages: (i) they inject traffic at speed, as is the case of SafeTI, and (ii) can be used on COTS MPSoCs, which SafeTI cannot do since it needs to be deployed in the MPSoC. 
However, those benchmarks also bring some disadvantages, which relate to (a) poor diversity derived from their inability to inject particular traffic patterns (e.g., variable burst size) due to ISA limitations, as Figure~\ref{fig:demo_waves} suggests for the CPU; (b) poor controllability given that software-induced activity must first traverse some stages of a pipelined core as well as some cache memories before producing traffic in specific interconnects; (c) long-burst traffic that can only be generated by components other than the CPU (e.g., DMA, Ethernet) occurs asynchronously in terms of timing and burstiness w.r.t. the CPU traffic, and hence, offer low controllability; and (d) poor observability means to tell whether the intended traffic patterns have been effectively injected.

SafeTI does not suffer those disadvantages thanks to its highly-programmable traffic range, allowing traffic diversity with virtually any duration and shape limited only by the communication protocol; it can inject synchronously, hence providing increased controllability; and with higher observability as measured by the statistics (e.g., request counter) included in SafeTI.


\subsection{Off-Chip Network Related Traffic Generation}
Abundant work on traffic generation, in contrast to our work, is tailored for off-chip network testing~\cite{ETH_TRAFFIC} or network communication enhancement~\cite{DCA2}, usually Ethernet. 
This type of work focuses on the off-chip traffic, whose characteristics are too different from those of on-chip traffic, hence making them not relevant for our context. This relates mostly to the fact that off-chip traffic often operates at larger time scales, and even asynchronously. Instead, on-chip traffic is relevant at the cycle level, and most components interacting in the SoC often work with the same clock in a synchronous manner, so the effect of traffic injection on-chip may change significantly due to small time shifts across injections and also by slightly varying the characteristics of the traffic injected. Hence, techniques for off-chip traffic injection are generally orthogonal to those needed for on-chip traffic injection.

Only a few works targeting off-chip traffic management focus on on-chip effects. For instance, some authors propose activating Direct Cache Access (DCA) in an 8-core MPSoC~\cite{DCA_EVAL} when running 10GbE TCP/IP Ethernet multi-thread workloads, showing significant performance gains.
DCA~\cite{DCA1} consists of a coherence protocol optimization where the network interface controller delivers data to a CPU-cache, resulting in a generalized data access latency decrease during large off-chip network data transfers. 
However, this work focuses on a specific performance optimization but provides no solution for controlled traffic injection, as opposed to SafeTI.
