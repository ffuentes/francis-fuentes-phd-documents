\section{Evaluation}
\label{sec:eval}
This section assesses the effectiveness and flexibility of SafeTI to inject traffic in the on-chip network, creating contention and hence impacting the execution time of a task under analysis running in one core.
Section~\ref{sec:eval_frame} presents the evaluation framework, Section~\ref{sec:eval_cores} evaluates existing software-only solutions showing their limited capabilities, Section~\ref{sec:eval_safeti} shows how SafeTI is able to inject a much wider variety of programmable traffic patterns than existing solutions, and Section~\ref{sec:eval_tacle} demonstrates SafeTI effectiveness as a contender against several software programs.

\subsection{Evaluation Framework}
\label{sec:eval_frame}
The evaluation has been performed on the evaluation platform presented in Section~\ref{sec:platform}.
In particular, we work on a bare-metal synthesis of the MPSoC platform on the Xilinx Virtex UltraScale+ VCU118 FPGA-based evaluation kit~\cite{FPGA}, operating at a frequency of 100 MHz.
Software programs run on the cores have been written in C and compiled with Frontgrade Gaisler AB's NCC GCC Bare-metal toolchain version 1.0.4 on a Linux system with an O2 optimization level for a RISC-V target, using disjoint memory regions for each core.
Programs are loaded into memory using the FPGA debug software GRMON3~\cite{GRMON} with each core set with a specific pointer during the platform booting, matching the program compilation pointers.
One of the programs is designed to operate one core of the MPSoC platform to perform the required SafeTI programming and execute a Task Under Analysis (TUA) while contended by either other programs running on the other cores or by SafeTI.

The metric of interest in this evaluation is the contention capacity achievable on the TUA execution by delaying access, to the L2 cache in the AHB network for instance, due to other contenders, CPU, or SafeTI, traffic.
Controlling the TUA execution progress to measure the contention achieved at each part of the test without altering the contention measure is a challenge in itself. Hence, we resort to programs, commonly named stressing tests, specifically designed to access with constant L2 misses, such as the TUA. Those programs have a repetitive and known beforehand behavior, which eases the analysis and interpretation of results.
The TUA execution is timed with an external counter using the statistical unit, SafeSU ~\cite{SafeSU}, by resetting and enabling the counter previous to the start, and disabling it after the TUA execution, hence delivering the total execution time of the TUA.
In-core performance counters have not been used to ensure the TUA has finished all transactions since SafeSU access is executed through the AHB interface, where transactions can be monitored, whereas core performance counters do not since the program may finish while it still has a number of write requests in the core pending to be sent to the AHB interface.

\begin{figure}[t!]
  \centering
  \includegraphics[width=1\textwidth]{img/test_plan_ahb.png}
  \caption{Partial example of the experiments carried, depicting the AHB actors capable of generating traffic, cores 0 to 5 and SafeTI AHB, and their programming, LD\_L2MISS or ST\_L2MISS for the cores and read and write traffic for SafeTI, for each case of Figure~\ref{fig:plru_data}.}
  \Description{Figure 10 includes 5 different experiments based on AHB contention. The first in blue, core 0 executes the TUA LD\_L2MISS while SafeTI injects READ transfers, while the second in orange injects WRITE transfers. The third in green, core 0 executes the TUA ST\_L2MISS while SafeTI injects READ transfers, while the fourth in red injects WRITE transfers. The fifth case has core 0 executing LD\_L2MISS while core 1 to 5 executes ST\_L2MISS in loop. Any actor not mentioned for every case is considered idle, thus, no generating interference traffic.}
  \label{fig:test_plan}
\end{figure}

For the sake of clarity, Figure~\ref{fig:test_plan} diagram exemplifies some configurations evaluated in our experiments, specifically the ones shown in Figure~\ref{fig:plru_data}. In detail, the first experiment configuration, indicated with the blue line, includes core 0 executing LD\_L2MISS while the other cores, CPU~1 to 5, are set to idle and SafeTI injects read transfers continuously on the AHB network, accessing the L2~cache.
All three posterior experiments follow a similar template, changing the SafeTI to write transfers for the orange line, and core 0 execution to ST\_L2MISS with SafeTI read and write traffic for the green and red lines, respectively. Finally, the fifth experiment includes core 0 executing LD\_L2MISS while cores 1 to 5 execute ST\_L2MISS, and SafeTI is idle.

Using stress tests that hit in the L2 cache could make that interfering traffic (i) delays accesses of the TUA, as wanted, and (ii) evicts L2 cache lines causing misses in the TUA and contending tasks. The latter would jeopardize our ability to take apart contention due to delays in the access to the L2 cache and memory. Hence, we focus on the use of programs that always miss in the L2 cache so that mutual interference is only caused due to delays in the accesses performed, but without altering the accesses performed. Moreover, all actors (cores and SafeTI) access disjoint memory regions to avoid undesired hits due to unwanted data reuse across actors.

The benchmarks used both as TUA and as contenders are \textit{LD\_L2MISS} and \textit{ST\_L2MISS}, which perform sustained loads and stores, respectively. Both programs access memory with a stride of 32~B, matching the cache line size, and access 2\textsuperscript{15} addresses separated by exactly those 32~B. Hence, they access 1~MB of memory (twice the size of the L2 cache) with unique accesses that always miss in the L2 cache. In the case of the TUA, the corresponding benchmark performs those accesses once, whereas contenders are executed in a loop to guarantee that they are constantly accessing memory while the TUA is run.


\subsection{MPSoC and SafeTI Equivalent Contention}
\label{sec:eval_cores}
For this first set of results, we consider the contention that can be injected with software-only means with one stressing benchmark. For that purpose, we run the TUA in core 0 and the software contender in core 1. Figure~\ref{fig:cores_data} shows the results of such a comparison. In particular, we consider two different benchmarks: LD\_L2MISS and ST\_L2MISS, as described before. The particular benchmark used as TUA and as software (SW) contender in each experiment is shown in the top x-axis. For each combination of TUA and SW contender benchmarks, we report results running the TUA in isolation (\emph{isolation}), with the SW contender in core 1 (\emph{1 SW contender}), and two traffic patterns for SafeTI (\emph{SafeTI-1} and \emph{SafeTI-2}), which we discuss later. Finally, for each combination of benchmarks and contenders, we show results using pRandom and pLRU replacement policies in the L2 cache. While differences among pRandom and pLRU are tiny in this subsection, both sets of results are included for completeness since differences arise, instead, in the next subsection.
To ease readability, all results have been normalized with respect to the case where the TUA in question runs in isolation.

\begin{figure}[t!]
  \centering
  \includegraphics[width=1\textwidth]{img/cores.png}
  \caption{Normalized execution time of one core executing the TUA in isolation, with one contender core or with SafeTI contention, for each combination of TUA and contender program.
  For instance SafeTI-1, the injector is programmed to execute a traffic pattern analog to the contender program, while in SafeTI-2, has an increased access size to match the execution time of the contention core executing the non-blocking traffic program ST\_L2MISS.}
  \Description{Ordering from TUA first and contender program second; LD_L2MISS and LD_L2MISS shows an increment of the 80 \% the execution with 1 contender, where SafeTI-1 adapts closely and SafeTI-2 overshoots with a 160 \%; LD_L2MISS and ST_L2MISS shows an increment of 260 \%, where SafeTI-1 is short with a 130 \% and SafeTI-2 adapts closely; ST_L2MISS and LD_L2MISS shows an increment of 50 \%, where SafeTI-1 adapts closely and SafeTI-2 overshoots with a 100 \%; ST_L2MISS and ST_L2MISS shows and increment of 150 \%, where SafeTI-1 is short with a 70 \% and SafeTI-2 adapts closely.}
  \label{fig:cores_data}
\end{figure}

When comparing isolation results only, we note that the L2 replacement policy has a limited impact on execution time, and using ST\_L2MISS instead of LD\_L2MISS as TUA slightly increases execution time.

When comparing isolation results against 1 SW contender, we note that execution time increases noticeably, and such an increase varies across combinations. First, we note that whenever ST\_L2MISS is used as a contender, the execution time increase of the TUA is significant. This relates to the fact that loads are blocking where, as previously explained, a load miss stalls the program until served. Stores instead are non-blocking, so further store misses can be generated before the first one is served. Hence, the TUA either competes against one load L2 miss or against multiple stores L2 misses. Therefore, the latter case can create higher contention on the TUA (between 2.7 and 3.8x slowdown).

We also note that whenever the TUA is an LD\_L2MISS, it experiences higher contention than whenever it is a ST\_L2MISS. If it is an LD\_L2MISS, all its execution time is consumed waiting for the only pending load at a time being served. Hence, any contention experience has a direct impact on execution time. Conversely, if the TUA is a ST\_L2MISS benchmark, stores are partially overlapped, and hence, they make progress in parallel and experience contention in parallel so that some cycles of contention are shared across stores and the total contention time experienced is lower than for the LD\_L2MISS counterpart.

Hence, the highest contention occurs when the TUA is an LD\_L2MISS and the SW contender a ST\_L2MISS, whereas the lowest contention occurs in the symmetrical case, i.e., when the TUA is a ST\_L2MISS and the SW contender an LD\_L2MISS.

Since the SafeTI is a blocking traffic injector, one injection does not start until the previous one finishes. Hence, if each traffic injection performs a single load L2 cache miss, it resembles an LD\_L2MISS benchmark. Instead, suppose each traffic injection performs multiple store L2 cache misses (e.g., by storing data in multiple L2 cache lines). In that case, it will resemble a ST\_L2MISS benchmark because those multiple accesses are sent without any type of serialization in its source (in the SafeTI). In particular, we have observed that accessing 2 cache lines is enough to mimic the behavior of ST\_L2MISS, though this effect is specific to low-size injections, similar to those produced by the cores (e.g., when running a ST\_L2MISS benchmark). Larger write requests, instead, experience different delays on their way to the L2 cache or main memory.

In Figure~\ref{fig:cores_data}, SafeTI-1 corresponds to the case where 1 cache line is accessed, whereas SafeTI-2 to the case where 2 cache lines are accessed. Whether reads or writes are used depends on what the SW contender does. If the SW contender is an LD\_L2MISS, SafeTI-1, and SafeTI-2 perform reads. Instead, if the SW contender is a ST\_L2MISS, SafeTI-1, and SafeTI-2 perform writes.

As expected, our results show that SafeTI-1 matches the slowdown produced on the TUA in the cases where the SW contender is an LD\_L2MISS benchmark, and SafeTI-2 matches the contention created by the ST\_L2MISS benchmark as an SW contender. This holds true regardless of the benchmark used as TUA and the L2 replacement policy used. For instance, in the LD\_L2MISS (TUA) vs LD\_L2MISS (SW contender) case, SafeTI-1 produces the same contention as the SW contender. Analogously, in the LD\_L2MISS (TUA) vs ST\_L2MISS (SW contender) case, SafeTI-2 produces the same contention as the SW contender.

Overall, our results show that SafeTI can easily match the contention produced by software-only means. Next, we will show that apart from mimicking benchmarks behavior, SafeTI can generate many more contention scenarios that SW-only injection would be extremely unlikely to produce due to the lack of controllability to enforce some behavior to occur simultaneously.


\subsection{Extended SafeTI Contention}
\label{sec:eval_safeti}
The second set of results corresponds to the contention generated by a single SafeTI module, without the injection constraints set in the previous subsection, to demonstrate the increased capabilities SafeTI has over other solutions. In this second part of the evaluation, we assume the same setup as for SafeTI experiments in the previous subsection: one core (the TUA) executes one of the two stressing programs, LD\_L2MISS or ST\_L2MISS, whereas the SafeTI generates traffic to the L2 cache with a specific access type, read or write, with all the other cores disabled.

The difference between the previous and this evaluation is that this one puts the focus on accessing increasingly large amounts of data (i.e., address ranges) in bursts mimicking the traffic pattern that some devices (e.g., an accelerator) could produce using a DMA controller, or the traffic that some I/O devices (e.g., PCIe or Ethernet controllers) could produce.
Note that, as explained before, CPUs are unable to produce the same diversity of traffic due to being limited by ISA and/or microarchitecture constraints. This is illustrated in previous Figure~\ref{fig:demo_waves}, where CPU cores generate, exclusively, accesses of up to 32~bytes (cache line size) in our testing scenario.
Hence, SafeTI is configured to inject in \textit{QUEUE} mode, where the programmed traffic pattern is repeated continuously until disabled after TUA completion. Note that by repeatedly accessing the same memory range, the SafeTI may cause abundant L2 hits, especially when accessing small data sizes fitting in the L2 cache, resulting in an increased variety of contention situations due to diverse access latency compared to the previous contention scenario (i.e., the one in the previous subsection).
For that purpose, SafeTI contention has been evaluated with a sweep of the data size accessed, using multiple descriptors whenever required.

\begin{figure}[t!]
  \centering
  \includegraphics[width=.5\textwidth]{img/pLRU.png}\includegraphics[width=.5\textwidth]{img/pRand.png}
  \caption{Program execution time on SafeTI AHB injection access size sweep for each TUA and traffic injection type using the pLRU (left) and pRandom (right) cache replacement policies. The horizontal axes are in logarithmic base two for clarity (each tick represents a power of two), while the purple line indicates the maximum contention achieved with all available SW-only contender cores for reference.}
  \Description{Figure explained in detail within the text. All SW-only contender cores contention is only surpassed after reaching an injection size of 512~KB, the total L2 cache capacity, in both pLRU and pRandom replacement policies.}
  \label{fig:plru_data}
  \label{fig:prandom_data}
\end{figure}

Figure~\ref{fig:plru_data} shows the execution times for the L2 cache pLRU and pRandom replacement policies on the left and right, respectively, during the data size sweep for the contention injected by the SafeTI. Each of the four lines corresponds to one of the combinations of LD\_L2MISS and ST\_L2MISS for the TUA vs read and write transactions for the SafeTI.
Note that the horizontal axis in both figures is in logarithmic scale for clarity, with a power of two on each axis ticks. The purple line marks the maximum execution time achieved using SW-only contender cores for reference (i.e., a ST\_L2MISS running in each of the 5 contender cores).

We start focusing first on the pLRU case (see Figure~\ref{fig:plru_data}, left). Our first observation is that the impact of contention across the four combinations of TUA and SafeTI keeps a similar ordering to the previous contention scenario across all data sizes. In particular, the lowest contention occurs when the TUA runs ST\_L2MISS and SafeTI injects read operations, and the highest when the TUA runs LD\_L2MISS and SafeTI injects write operations. These best and worst contention scenarios are analogous to those in the previous analysis, where SafeTI mimics cores traffic patterns (small data size transfers).

The second observation relates to the pattern observed for the contention as we increase the data size accessed by the SafeTI. For this analysis, we focus on the scenario with an LD\_L2MISS TUA and write operations for the SafeTI (top orange line in the plot), although the same trend holds in all four combinations. First, we observe that, as we increase the data size accessed, contention increases until reaching 1~KB data size. This relates to the fact that the maximum data transfer size is 1~KB for AMBA AHB. Hence, load operations of the TUA are interleaved with increasingly long write requests of the SafeTI, therefore increasing contention since longer data transfers keep the AHB bus busy for longer time. Still, overall contention is low because data accessed by the SafeTI is small and fits comfortably in the L2 cache. Hence, only some contention is introduced in the access to the L2~cache, but L2~cache latency for hits is low enough so that contention is limited even if each 1~KB transfer becomes 64~L2~hits into 32~cache lines of 32~bytes each.

\begin{figure}[t!]
  \centering
  \includegraphics[width=0.75\textwidth]{img/wave_size_saw_notscale.png}
  \caption{Intertwined AHB network ownership diagram, not-at-scale, during LD\_L2MISS TUA for data injection accesses of 128, 512, 1024, 1152, 1536 and 2048~bytes. TUA accesses marked in yellow and SafeTI accesses marked in light salmon.}
  \Description{SafeTI accesses are split, leaving access slots to the CPU in-between of 16 bytes, when the injection size is over 1 KB.}
  \label{fig:wave_size_saw}
\end{figure}

Right after reaching 1~KB, we note that contention decreases suddenly. This relates to the fact that, since the maximum transfer size is 1~KB, whenever such size is exceeded, data transfers are split into transfers of up to 1~KB. Hence, whenever transferring 1152~bytes, such transfer is split by the AHB interface into a 1~KB transfer followed by a 128~bytes transfer as Figure~\ref{fig:wave_size_saw} shows. Since a load of the TUA is issued in between each SafeTI transfer in the AHB interface, we move from delaying each load by the time needed to write 1~KB, to the case where those loads are delayed alternatively by 1~KB and 128 bytes SafeTI transfers. Hence, half of the loads experience significantly lower contention. As we approach 2~KB, contention approaches that of 1~KB, which is matched when exactly 2~KB are transferred. This pattern repeats with a lower relative impact as we increase the data size transfer. For instance, when transferring~4224 bytes, such transfer is split into 4 transfers of 1~KB and one of 128~bytes. Hence, for every 5 loads of the TUA, 4 of them experience maximum contention whereas the other experiences much lower contention. Therefore, contention approaches asymptotically that of 1~KB transfers with a serrated shape until approaching the limit of the L2 cache capacity (512~KB).

Whenever approaching the L2 cache size, and even before reaching such size, L2 cache misses of the TUA start evicting data fetched by the SafeTI that would lead to hits otherwise. In particular, below 384~KB (the size of 3 of the 4 cache ways), the SafeTI fills 3 cache lines per set. The TUA performs an eviction in each set periodically, but since the TUA accesses one line in each access whereas SafeTI accesses many due to its bursty traffic pattern, by the time the TUA accesses a given L2 set again, the SafeTI has already accessed the 3 cache lines it had in that set with 3 hits in many cases, moving them away from the LRU position.
Above 384~KB and until 512~KB, the TUA should be able to evict one cache line of the TUA (the one to be accessed next), which would produce a cascade effect with the SafeTI evicting the next line to be fetched in each access in any set with 4 cache lines allocated by the SafeTI. This behavior would always be this way if the replacement policy were pure LRU. However, since the replacement policy is pLRU, some variability may occur,provoking the third line in the LRU stack to be evicted instead of the fourth one, which may lead to extra misses in sets with 3 useful cache lines for the SafeTI, and to extra hits in sets with 4 useful cache lines for the SafeTI. However, when reaching 640~KB data transfers, the size of 5 cache ways, all SafeTI accesses become misses due to L2 capacity limitations.

By becoming L2 cache misses, the SafeTI accesses generate much higher contention on the TUA accesses that, instead of having to wait for some (fast) L2~cache hits to be served, they have to wait for some (slow) DRAM memory accesses to be served. Hence the reason why the slowdown caused by the TUA grows from around 5x to around 45x. In particular, SafeTI write traffic causes around 2x slowdown than that caused by read traffic since each write request leads to two DRAM transactions: one to write back dirty data evicted and another to fetch new data. Instead, read traffic only has to fetch new data since the data evicted is not dirty.

Note that the same trends are observed in the other scenarios where the TUA performs stores or the SafeTI performs reads, yet with lower overall contention, as discussed before.

When considering the pRandom replacement policy, shown in the right plot in Figure~\ref{fig:prandom_data}, we observe similar trends to those observed with pLRU with the difference that, even below 384~KB data accesses, some data fetched by the SafeTI may be evicted, hence increasing contention with respect to the pLRU counterpart. Also, accesses beyond 640~KB may still allow some useful data for the SafeTI to survive in the L2 cache and lead to hits, which generate lower interference in the TUA. Hence, the same slowdowns reached with pLRU sharply are reached smoothly and asymptotically with pRandom. Still, the effectiveness and flexibility of the SafeTI to to create contention through traffic injection become obvious in the light of these results.


\subsection{Complex Scenarios with Multiple SafeTI Modules}
\begin{figure}[t!]
  \centering
  \includegraphics[width=0.55\textwidth]{img/selene_axi.png}
  \caption{SELENE MPSoC platform architecture with two SafeTI integrated modules at the AHB and AXI4 interconnect levels, respectively.}
  \Description{Same platform as original layout, but including a second SafeTI with injection access directly to DRAM through the AXI4 interconnect. This second module is also programmed through APB, at different memory space.}
  \label{fig:platform_axi}
\end{figure}

To illustrate SafeTI scalability and flexibility, we integrate a second SafeTI module in the AXI4 network as shown in Figure~\ref{fig:platform_axi}.
The implementation required to instantiate SafeTI in a new interface is relatively easy since most of its components do not require modification. In particular, only an AXI4 interface needs to be developed for targeting the corresponding AXI4 network.
Traffic pattern programming works analogously to the case of the AHB instantiation since it is also programmed through the APB bus, and traffic descriptors remain unaltered. The only difference is that the new SafeTI module is mapped to a different memory space so that both SafeTI modules can be programmed separately. Since both modules are physically independent (and replicated), they can inject traffic simultaneously in both AHB and AXI4 interconnects, at the cost of doubling the footprint on the platform implementation.

Some specific applications of the SafeTI could require precisely synchronized injection across both interfaces. Since they have been instantiated as two independent modules, such type of synchronization is not possible. Devising a SafeTI architecture allowing such synchronized injection across multiple interfaces simultaneously is beyond the scope of this work. In this paper, we instead focus on the single-interface (either AHB or AXI4 in this case) realization of SafeTI, which is a highly flexible solution that allows injecting different traffic patterns across different interconnect interfaces.

\begin{figure}[t!]
  \centering
  \includegraphics[width=.58\textwidth]{img/pLRU_axi.png}
  \caption{Program execution time on SafeTI AXI4 injection access size sweep for each TUA and traffic injection type. The horizontal axes are in logarithmic base two for clarity (each tick represents a power of two), while the purple line indicates the maximum contention achieved with all available SW-only contender cores for reference.}
  \Description{Figure explained in detail within the text. All SW-only contender cores contention is not surpassed by the contention at the AXI interconnect. A serrated waveform repeats every 4~KB, while all combinations follow the same curve but LD\_L2MISS and write traffic type contention.}
  \label{fig:axi_data}
\end{figure}

Figure~\ref{fig:axi_data} shows both TUA, LD\_L2MISS or ST\_L2MISS, execution times when contended solely by the SafeTI targeting the AXI4 interconnect when injecting read or write traffic varying the request size.
The maximum contention achieved by the traffic generated at the AXI level is observed to be a fraction of AHB-based contention (see Figure~\ref{fig:plru_data}), just reaching the contention equivalent to 5-CPUs from Section~\ref{sec:eval_cores}.
This relates to the fact that the AHB network has a single channel for read and write transactions, which produces significant serialization of the requests, and hence, high slowdown on the TUA. The AXI4 network, as explained next, implements separated read and write channels allowing some degree of parallel processing of different requests, and hence, causes lower contention due to serialization.

As shown in the figure, SafeTI in the AXI4 interconnect generates non-negligible interference in all cases but one. In particular, when the TUA performs read operations to DRAM and SafeTI performs write operations, the contention is almost negligible. This relates to the fact that the AMBA AXI4 protocol~\cite{amba_axi4} integrates a dual-channel architecture isolating read and write data transfers. Hence, interference when the TUA and SafeTI perform the same type of access (either both read or both write) is expected. Similarly, in the aforementioned case (TUA reading and SafeTI writing) no contention is expected. However, in the case when the TUA performs write operations and SafeTI read operations, we note that the L2 cache is write-allocate. Hence, upon each write access of the TUA, it also performs a read access to allocate the cache line in L2. Therefore, those read operations caused by the TUA experience contention caused by the read operations of SafeTI.

\begin{table}[t!]
  \centering
  \caption{Normalized TUA execution times under SafeTI 1~KB (Top) and 2~MB (Bottom) access size contention based on reads or writes and at AHB, AXI4 or AHB and AXI4 traffic injection}
  \begin{tabular}{|r|c|c|c|c|} \hline
  TUA                    & \multicolumn{2}{c|}{LD\_L2MISS} & \multicolumn{2}{c|}{ST\_L2MISS} \\ \hline
  Injection Type         & READ          & WRITE          & READ          & WRITE            \\ \hline
  AXI4                   & 2.4           & 1.1            & 2.0           & 2.0              \\ \hline
  AHB                    & 3.5           & 4.3            & 2.0           & 2.4              \\ \hline
  AHB + AXI4             & 4.6           & 4.5            & 3.0           & 3.2              \\ \hline
  \end{tabular}
  \begin{tabular}{|r|c|c|c|c|} \hline
  TUA                    & \multicolumn{2}{c|}{LD\_L2MISS} & \multicolumn{2}{c|}{ST\_L2MISS} \\ \hline
  Injection Type         & READ          & WRITE          & READ          & WRITE            \\ \hline
  AXI4                   & 7.5           & 1.1            & 6.6           & 6.7              \\ \hline
  AHB                    & 28.7          & 45.3           & 13.8          & 20.6             \\ \hline
  AHB + AXI4             & 246.7         & 448.3          & 131.6         & 269.8            \\ \hline
  \end{tabular}
  \label{tab:axi_data}
  \\
\end{table}

For completeness, Table~\ref{tab:axi_data} summarizes and extends this evaluation section with the normalized TUA execution times when contended by SafeTI generated traffic at the (1)AHB, (2)AXI4 or (3)AHB + AXI4 interconnects, for read or write traffic types with 1~KB (top) or 2~MB (bottom) request size, over the isolated TUA executions of LD\_L2MISS or ST\_L2MISS, respectively. These specific traffic access sizes have been selected to represent low and high contention profiles, showing the contention increase when programming a traffic pattern of wider data access.

For the setup with only AXI4 contention, the TUA execution time ranges between 2.0 and 7.5 times the execution time of the TUA in isolation depending on the access size programmed, except for the cases of LD\_L2MISS TUA (read traffic for the TUA) and write SafeTI traffic combination, where TUA's execution time remains almost unaffected (1.1x) due to the double-channel architecture of the AXI4 network, as previously explained.

In contrast, AHB traffic generation achieves higher contention levels due to the single-channel architecture of the AHB network.
Slowdown ranges between 2.0 and 4.3 times for the 1~KB access size of the SafeTI traffic, and 13.8 and 45.3 times for the 2~MB access size. The wide contention range of the larger access size results from interactions of write accesses on the L2~cache, requiring the eviction of dirty cache lines, as previously explained. This effect penalizes especially the LD\_L2MISS TUA which, in isolation, does not suffer this penalization, thus making it highly sensitive to the contention by the SafeTI write traffic.

Lastly, parallel traffic injection on both AHB and AXI4 interconnects with the same access size and type, results in slightly higher execution times for 1~KB access size, being between 3.0 and 4.6 times with respect to the TUA execution in isolation. However, a factor of around 10 over the AHB-only contention is observed for the 2~MB access size, being between 131.6 and 448.3 times with respect to the TUA executions in isolation. Normalized execution times for the cases of 2~MB traffic generation on both AHB and AXI4 can be reasoned analytically by building on in-depth knowledge of the platform specifications. In this case, such knowledge includes characteristics such as both AHB and AXI4 implementing a 128-bit data bus width, having a maximum burst access size of 1~KB and 4~KB, respectively, and integrating a round-robin scheduling algorithm for issuing data transfer requests.
For instance, considering the simplest case where the CPU is executing LD\_L2MISS, while both SafeTI modules inject read traffic of 2~MB, the CPU is contended first by the AHB traffic (1~KB), which in turn is contended by the AXI4 traffic (4~KB), and once the CPU request reaches the AXI4 network, it is again contended by the AXI4 traffic (4~KB), amounting to a total of 9~KB data transfer contention, or equivalent to 9 times the AHB traffic (1~KB) contention. Thus, one could expect a contention of around $28.7 x 9 = 258.3$ times the execution time in isolation when injecting traffic in both networks with SafeTI modules. The actual value observed is 246.7, which is within 5\% of the analytical value. Hence, we regard this value as expected.

Other cases could be analyzed following similar reasoning. However, non-trivial traffic dynamics, such as the interference caused by dirty line evictions that may affect both, TUA traffic as well as SafeTI traffic in both networks, with non-obvious interactions across such traffic, make analytical assessment impractical in general.
In fact, increasing MPSoC complexity, such as implementing networks with different bus widths, priority or dynamic scheduling algorithms (e.g., managed by quality-of-service features), among other architecture characteristics, further challenge any type of static analysis, making measurement-based empirical analysis (e.g., based on the use of SafeTI instances) the only viable alternative. This is particularly true on safety-critical platforms, where testing on the target hardware is compulsory for achieving complete validation.


\subsection{Real-world Software with SafeTI Contention}
\label{sec:eval_tacle}
Previously conducted experiments with L2~cache miss-based benchmarks as TUA provide a systematic and reliable behavior for facilitating analysis of contention effects in the evaluation platform.
This section complements those experiments using real-world benchmarks as TUA. In particular, we use the same baseline environment, which includes the platform from Figure~\ref{fig:platform}, with the same framework of SafeTI-based contention with AHB-level traffic injection, using several benchmarks from the taclebench suite~\cite{tacle} as TUA with diversity in their data access patterns, with some of them mostly hitting in L1 cache, some others in L2 cache.

\begin{figure}[t!]
  \centering
  \includegraphics[width=.91\textwidth]{img/taclebench.png}
  \caption{Normalized execution times of 100 benchmark executions under SafeTI read and write 1~KB and 2~MB access size AHB based contention relative to execution in isolation on the pLRU L2~cache platform implementation.
  }
  \Description{}
  \label{fig:taclebench_data}
\end{figure}

Considering the case where the L2~cache implements a pLRU replacement policy, Figure~\ref{fig:taclebench_data} shows the normalized execution times with respect to each taclebench execution without contention, i.e., in isolation. The results include 100 executions per benchmark, considering accumulated execution times to discount sporadic abnormal variations, such as those observed in the first execution when cache memories are not warmed up.

Results show that different benchmarks are affected differently by AHB traffic contention, with some being particularly insensitive (e.g., \emph{adpcm-dec}, \emph{petrinet}) whereas others are highly sensitive (e.g., \emph{cjpeg-transupp}, \emph{statemate}). The disparity across the slowdown experienced by different benchmarks indicates those benchmarks have different memory access patterns, resulting in a diverse range of results with different contention levels.

Benchmarks LD\_L2MISS and ST\_L2MISS used in previous sections were designed to systematically miss on the L2~cache, forcing them to access main memory.
Conversely, using SafeSU performance counters, we have validated that all accesses in the taclebench hit in L1 or L2~cache except the cold misses (i.e., first access to each cache line). Therefore, those benchmarks are expected to behave differently to the benchmarks evaluated before, hence complementing the previous evaluation.
In general, while each benchmark typically combines L1 hits, L2 hits and L2 misses, if their behavior is dominated by one of these patterns, we expect to find the following three cases:
\begin{enumerate}
\item No L2~cache access (due to L1~cache hit). In this case, since requests to L2 are scarce, the contention experienced is rather low. For instance, \emph{petrinet} is the most representative case of this behavior.
\item L2~cache miss access, as discussed before, is the analyzed with LD\_L2MISS and ST\_L2MISS benchmarks. Therefore, we have not sought additional benchmarks for this scenario since L2 miss accesses dominate the execution and behavior observed would be almost identical to that of LD\_L2MISS and ST\_L2MISS benchmarks.
\item Finally, L2~cache hit access is the scenario where contention experienced can be the highest. In particular, SafeTI traffic can evict program data from the L2, hence transforming L2 hits into L2 misses with contention, which can cause much higher relative slowdowns. Both \emph{cjpeg-transupp} and \emph{statemate} are the most evident examples of this behavior with slowdowns exceeding 10x and 100x for the 1~KB and 2~MB SafeTI injection sizes, respectively. Those values exceed by far the highest slowdowns observed in Table~\ref{tab:axi_data} for a TUA that misses in L2 (up to 4.3x and 45.3x for 1~KB and 2~MB SafeTI injection sizes, respectively).
\end{enumerate}
