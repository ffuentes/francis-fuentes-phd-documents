February 14th of 2024 meeting

Topics discussed:
- Prove of lack of inclusive policy.
- Reason the SafeTI SCA protection when lacking inclusive policy.


Work to do in the following week/s:
- Write ERTS2023 SCA protection paper.
- Study focused protection limitations (+calibration in the future).


Future work:
- Update SafeTI documentation (drivers + AXI).
- Research and implement multiple SafeTI interactions.
- Implement BRANCH and SYNC descriptors.
