December 4th of 2024 meeting

Topics discussed:
- Debug of SafeTI Observer implementation for cycle-accurate traffic generation.
- Re-design of execution pipeline.
- Possible global clock trigger implementation for the future (system avionics).


Work to do in the following week/s:
- Validation of the hardware changes.
- Coherence validation program.


Future work:
- Update SafeTI documentation (drivers + AXI).
- Research and implement multiple SafeTI interactions.
- Implement BRANCH and SYNC descriptors.
