Reunión 14 de septiembre del 2022

Temas tratados:
- Presentar puntos principales los cuales pueden ser sujeto del paper.
- Mostrar incoherencias en los datos del paper previo [1], aka ancho de bus
  de datos interno distinto al del AHB, generando contención "falsa".
  - Debido a la cantidad de cambios, nuevas características y el tema de los
    resultados del paper anterior, se ha decidido que este paper se va a centrar
    en la red AHB de la plataforma actual con cache L2.
    Dependiendo de cómo quede el paper, se podría incluir interfaz AXI y datos,
    pero lo más probable es que se deje para un futuro paper.

[1] O. Sala et al., "SafeTI: a Hardware Traffic Injector for MPSoC Functional
    and Timing Validation," 2021 IEEE 27th International Symposium on On-Line
    Testing and Robust System Design (IOLTS), 2021, pp. 1-7,
    doi: 10.1109/IOLTS52814.2021.9486689.

Trabajo a realizar en estas siguientes semanas:
- Fijar los puntos principales del paper en LaTeX y compartir con el grupo CAOS
  el repositorio para poder avanzar en paralelo.


Futuro trabajo:
- Actualizar documentación con los cambios del inyector e interfaz AXI.
- Ver la interacción de múltiples inyectores en la misma o distintas redes
  interconectadas.
