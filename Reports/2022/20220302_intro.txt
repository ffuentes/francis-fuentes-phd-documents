Temas tratados:

    Contexto del injector como futura herramienta de Validacion y Verificacion (V&V) en sistemas de operacion critica (validar coherencia de cache en redes, entre otros).
    Estado actual del injector (herramienta logica con algunos problemas para sintetizacion en FPGA, mayormente resueltos aunque falta por ver).
    Plataforma SELENE como lugar de desarrollo del injector, junto a otras herramientas como el SafePMU (Performance Monitor Unit, usado para medir la contencion debida por el trafico en el bus, que a su vez puede ser utilizado para validar el funcionamiento del injector en la red del SoC).
    Estado actual de la interfaz AXI:
        Validacion limitada y manual han sido realizadas.
        Las caracteristicas implementadas son limitadas respecto todo el protocolo AXI, para asi centrarse unicamente en aquellas que puedan ayudar a injectar trafico.
        Incluye opcion para implementar solo la parte necesaria de la interfaz para el funcionamiento del injector, aunque tambien se puede utilizar como una interfaz AXI de proposito general para otros proyectos.

Trabajo a realizar en estas siguientes semanas:

    Acabar la documentacion de la interfaz AXI para entregar al equipo de Valencia (UPV) y que puedan empezar integracion de injector+interfaz en sus proyectos. <- Principal.
    Realizar modificaciones minimas en el injector para sacar el maximo partido a la interfaz (senal para realizar el bypass de cuellos de botella). <- Secundario.
    Estudiar herramientas de validacion formal de protocolo AXI para verificar la interfaz (validar unicamente las caracteristicas implementadas). <- Secundario.

Futuro trabajo:

    Aprender la metodologia correcta de sintetizacion hardware en FPGA (aplicar constraints durante sintetizacion).
    Realizar la integracion total del injector+interfaz AXI en plataforma SoC (SELENE) y sintetizarlo en FPGA para obtener datos (obtener una version estable del injector, revisando y acabando algunas funcionalidades por implementar, como el reset y pausa de la programacion del injector).
    Escribir paper actualizando la informacion respecto injector, expandiendolo con el anadido de la interfaz AXI (se necesita lo anterior).
    Ver la interaccion de multiple injectores en la misma o distintas redes interconectadas para validar coherencia en las redes. Posiblemente implementar comunicacion entre distintos injectores o expandir el injector para transmitir por multiple interfaces con opciones programables de delay y tipo de operacion segun interfaz, opciones a estimar y discutir.