Reunión 29 de noviembre del 2022

Temas tratados:
- Plan de investigación finalizado.


Trabajo a realizar en estas siguientes semanas:
- Revisar SafeTI AHB paper con las sugerencias de Raimon.
- Presentar futuros puntos para expandir SafeTI.


Futuro trabajo:
- Actualizar documentación con los cambios del inyector e interfaz AXI.
- Ver la interacción de múltiples inyectores en la misma o distintas redes
  interconectadas.
- Añadir la capacidad de mantener peticiones no bloqueantes en la interfaz AHB
  (llamado en presentaciones "cached requests") del SafeTI. Esto incrementaría
  contención en tests de ST_L2MISS.
