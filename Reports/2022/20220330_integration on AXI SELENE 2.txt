Reunion 30 de marzo del 2022

Temas tratados:
- Funcionamiento del modulo SafeTI en la plataforma SELENE, tanto variantes AHB como AXI.
- Estado actual del SafeTI AXI en SELENE:
    - Funciona correctamente en lecturas, pero en escrituras falla si el burst supera los 256 bytes.
    - El SafeTI no funciona correctamente cuando se utiliza el modo optimizado de la interfaz AXI.
- Intento de utilizar Questa VIP para validacion de la interfaz AXI ha fallado. La herramienta no carga libreras y la documentacion/busqueda de ayuda no ha dado resultados.

Referencias:
- SafeTI AXI GitLab: https://gitlab.bsc.es/caos_hw/hdl_ip/bsc_safeti/-/tree/ft/axi_injector
- SafeTI documentation: https://gitlab.bsc.es/caos_hw/hdl_ip/bsc_safeti/-/blob/ft/axi_injector/docs/Injector_Specs.pdf
- SELENE branch for SafeTI AXI integration: https://gitlab.bsc.es/selene/selene-hardware/-/tree/bsc/safeti_axi
- SELENE branch for SafeTI AHB integration: https://gitlab.bsc.es/selene/selene-hardware/-/tree/BSC/ft/SafeTI

Trabajo a realizar en estas siguientes semanas:
- Identificar y solucionar los problemas del SafeTI AXI en plataforma SELENE. <- Principal.
- Revisar funcionamiento del SafeTI y hacerlo compatibles con la optimizacion de la interfaz AXI. <- Principal
- Portar y actualizar documentacion de la interfaz AXI a LaTeX. <- Secundario.
- Validacion formal de protocolo AXI para verificar la interfaz con Xilinx VIP. <- Secundario.

Futuro trabajo:
- Sintetizar y obtener datos del inyector en la plataforma SELENE.
- Escribir paper actualizando la informacion respecto injector, expandiendolo con el anadido de la interfaz AXI.
- Ver la interaccion de multiples inyectores en la misma o distintas redes interconectadas.