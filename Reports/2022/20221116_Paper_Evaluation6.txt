Reunión 16 de noviembre del 2022

Temas tratados:
- Mostrar datos actualizados (SafeTI y MPSoC contention) utilizando ubenchmarks
  en los que el acceso a dos veces el tamaño de L2 cache se realiza una vez, no
  dos como previamente se hacía. Esto evita L2HIT en ubenchmark que debería de
  hacer solo L2MISS, proporcionando datos más ajustados a la realidad.
- Explicar diferencia en ejecución de ubenchmark de lecturas y escrituras, y
  cómo eso afecta a la contención (en escrituras se cuelan dos accesos por ser
  no bloqueantes, mientras en lecturas solo una por ser bloqueantes).


Trabajo a realizar en estas siguientes semanas:
- Secciones de SafeTI, Plataforma y Evaluación estarán explicadas para este viernes.


Futuro trabajo:
- Actualizar documentación con los cambios del inyector e interfaz AXI.
- Ver la interacción de múltiples inyectores en la misma o distintas redes
  interconectadas.
- Añadir la capacidad de mantener peticiones no bloqueantes en la interfaz AHB
  (llamado en presentaciones "cached requests") del SafeTI. Esto incrementaría
  contención en tests de ST_L2MISS.
