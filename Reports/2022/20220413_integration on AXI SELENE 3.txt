Reunión 13 de abril del 2022

Temas tratados:
- Topología actualizada con la Performance Monitor Unit.
- Explicación y primeros tests en síntesis de la plataforma SELENE con el injector SafeTI AXI.
- La memoria de simulación difiere de la sintentizada, haciendo que el límite de 16 beats en AXI no exista en síntesis.

Trabajo a realizar en estas siguientes semanas:
- Identificar y solucionar los problemas de la síntesis para obtener datos. <- Principal.
- Revisar funcionamiento del SafeTI y hacerlo compatibles con la optimizacion de la interfaz AXI. <- Secundario
- Portar y actualizar documentacion de la interfaz AXI a LaTeX. <- Secundario.
- Validacion formal de protocolo AXI para verificar la interfaz con Xilinx VIP. <- Secundario.

Futuro trabajo:
- Escribir paper actualizando la informacion respecto injector, expandiendolo con el anadido de la interfaz AXI.
- Ver la interaccion de multiples inyectores en la misma o distintas redes interconectadas.