September 13th of 2023 meeting

Topics discussed:
- Unfocused batch SCA data.
- First SCAs with different cache topologies.


Work to do in the following week/s:
- Test other calibration components so the calibration results match with SCA.
- DFT presentation.


Future work:
- Update SafeTI documentation (drivers + AXI).
- Research and implement multiple SafeTI interactions.
- Implement BRANCH and SYNC descriptors.
