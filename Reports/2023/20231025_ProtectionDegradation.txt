October 25th of 2023 meeting

Topics discussed:
- No output differences were found between original and modified Bernstein code.
- Delay value range for optimal protection.
- Indications of protection degradation even at optimal Delay values.


Work to do in the following week/s:
- Find degradation rate on data for estimating the expected degradation on optimal protection.
- Write paper outline.
- Study focused protection limitations (+calibration in the future).


Future work:
- Update SafeTI documentation (drivers + AXI).
- Research and implement multiple SafeTI interactions.
- Implement BRANCH and SYNC descriptors.
