September 20th of 2023 meeting

Topics discussed:
- First version of DFT 2023 slides for short presentation (8 min).


Work to do in the following week/s:
- Test other calibration components so the calibration results match with SCA.
- DFT presentation.


Future work:
- Update SafeTI documentation (drivers + AXI).
- Research and implement multiple SafeTI interactions.
- Implement BRANCH and SYNC descriptors.
