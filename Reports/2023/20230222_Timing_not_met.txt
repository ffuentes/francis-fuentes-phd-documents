February, 15th of 2023 meeting

Topics discussed:
- Implemented and present data with the new request and traffic pattern
  completion counters, which they seem to corroborate previous data.
- Checked the logs from bitstream synthesis and found out the timing
  requirements were not met. Implemented the SafeTI on latest Selene platform
  version and data changed, but not to the point of being fully reasonable.


Work to do in the following week/s:
- Replicate SCA on SELENE.
- Execute tests where SafeTI is isolated to observe if there is same behavior.


Future work:
- Update SafeTI documentation (drivers + AXI).
- Research and implement multiple SafeTI interactions.
- Implement BRANCH and SYNC descriptors.
